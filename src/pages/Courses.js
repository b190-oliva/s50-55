import { Fragment, useEffect, useState } from 'react';
import CourseCard from "../components/CourseCard.js";
// import coursesData from "../data/coursesData.js";
export default function Courses(){

	const [ courses, setCourses] = useState([]);

	useEffect(()=>{
		fetch('https://tranquil-lake-75577.herokuapp.com/courses/all')
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			setCourses(data.map(course=>{
				return(
					<CourseCard key = {course._id} courseProp = {course} />
				)
			}));
		})
	},[])

	// const courses = coursesData.map(course=>{
	// 	return(
	// 		<CourseCard key = {course.id} courseProp = {course}/>
	// 	)
	// })

	return(
		<Fragment>
			{courses}
		</Fragment>
	)
}