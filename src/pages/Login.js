import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from 'sweetalert2';

export default function Login () {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState(''); 
	const [isActive, setIsActive] = useState(false);
	const { user, setUser } = useContext(UserContext);
	
	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details',{
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).then(res => res.json()).then(data => {
			console.log(data);
			setUser({
				id:data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[email,password]);

	function loginUser (e) {
		e.preventDefault();
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data => {
			console.log(data)
			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access);
			retrieveUserDetails(data.access);
			Swal.fire({
				title: "Login successful!",
				icon: "success",
				text: "Welcome to Zuitt!"
			})
			}
			else{
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});

		// localStorage.setItem('email', email);
		// setUser({
		// 	email: localStorage.getItem('email')
		// });
		setEmail('');
		setPassword('');
	}

	return(
		(user.id !== null)?
		<Navigate to = '/courses'/>
		:
		<Container fluid className = "w-50 my-5">
			<Form onSubmit = {(e)=>loginUser(e)}>
	      <Form.Group controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control
	        size ="sm" 
			    type="emk ail" 
			    placeholder="Enter email" 
			    value = {email} 
			    onChange = {e=>setEmail(e.target.value)}	
			    required/>
	        <Form.Text className="text-muted">
	        </Form.Text>
	      </Form.Group>

	      <Form.Group controlId="password">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password" 
	        	value = {password} 
			    onChange = {e=>setPassword(e.target.value)}
	        	required/>
	      </Form.Group>

	      {isActive ?
	      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn">
	        Login
	      </Button>
	      :
	      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn" disabled>
	        Login
	      </Button>
	  	}
	    </Form>
	  </Container>
  );
}