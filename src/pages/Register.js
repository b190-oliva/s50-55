import {useState, useEffect} from 'react';
import {Form, Button, Container} from 'react-bootstrap';
import { useNavigate } from "react-router";
import Swal from 'sweetalert2';

export default function Register () {
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);
	const [form, setForm] = useState({
	  firstname: "",
	  lastname: "",
	  email: "",
	  mobilenumber: "",
	  password: ""
	});
	const navigate = useNavigate();

	function updateForm(value) {
	   return setForm((prev) => {
	   	 setPassword2('');
	     return { ...prev, ...value };
	   });
	 }

	async function onSubmit (e){
		e.preventDefault();
		if(form.mobilenumber.length < 11){
			Swal.fire({
				title: "Minimum length for mobile number must be 11 digits or greater.",
				icon: "error",
				text: "Input your full mobile number"
			})
		}
		else{
			const newPerson = { ...form };
			await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(newPerson),
			})
			.catch(error=>{
				window.alert(error);
	     		return;
			}).then(res => res.json()).then(data => {
					if(data === false){
						Swal.fire({
							title: "Email already exists!",
							icon: "error",
							text: "Please try another email"
						})
					}
					else{
						Swal.fire({
							title: "Registration successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})
						navigate("/login");
					}
			})
			setForm({
			firstname: "",
			lastname: "",
			email: "",
			mobilenumber: "",
			password: ""
		});
		setPassword2('');
		}
	}

	useEffect (()=>{
		if(password2 === ""){
			return;
		}
		else if(form.password === password2){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[form,password2]);

	return (
		<Container fluid className = "w-25 my-5">
		    <Form onSubmit = {onSubmit}>
		      <Form.Group controlId="firstname">
		        <Form.Label>First Name</Form.Label>
		        <Form.Control 
				    type="text" 
				    placeholder="Enter your first name"
				    maxLength = "30" 
				    value = {form.firstname} 
				    onChange = {(e)=>updateForm({firstname: e.target.value})}	
				    required/>
		      </Form.Group>

		      <Form.Group controlId="lastname">
		        <Form.Label>Last Name</Form.Label>
		        <Form.Control 
				    type="text" 
				    placeholder="Enter your last name"
				    maxLength = "30" 
				    value = {form.lastname} 
				    onChange = {(e)=>updateForm({lastname: e.target.value})}	
				    required/>
		      </Form.Group>

		      <Form.Group controlId="email">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
				    type="email" 
				    placeholder="Enter your email"
				    maxLength = "40" 
				    value = {form.email} 
				    onChange = {(e)=>updateForm({email: e.target.value})}	
				    required/>
		      </Form.Group>
		      <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>

              <Form.Group controlId="mobilenumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                	type="number"
                	placeholder="Mobile number"
                	value = {form.mobilenumber} 
        		    onChange = {(e)=>updateForm({mobilenumber: e.target.value})} 
                	required/>
              </Form.Group>

		      <Form.Group controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Password"
		        	maxLength = "30" 
		        	value = {form.password} 
				    onChange = {(e)=>updateForm({password: e.target.value})}
		        	required/>
		      </Form.Group>

	            <Form.Group controlId="password2">
	              <Form.Label>Verify Password</Form.Label>
	              <Form.Control 
	              	type="password" 
	              	placeholder="Password"
	              	maxLength = "30"
	              	value = {password2} 
	      		    onChange = {(e)=>setPassword2(e.target.value)} 
	              	required/>
	            </Form.Group>
	          {isActive ?
		      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn">
		        Register
		      </Button>
		      :
		      <Button className= "mt-3" variant="primary" type="submit" id="submitBtn" disabled>
		        Register
		      </Button>
		  	}
		    </Form>
		</Container>
		
	);
}