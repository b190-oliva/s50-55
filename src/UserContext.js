import React from 'react';

const UserContext = React.createContext();

// createContext allows us to store a different data inside an object

export const UserProvider = UserContext.Provider;

// Provider component allows other component to use the context object and supply the necessary information

export default UserContext;