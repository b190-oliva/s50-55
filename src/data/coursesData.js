const coursesData = [
{
	id: "wdc001",
	name: "PHP-Laravel",
	description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ",
	price: 45000,
	onOffer: true,

},
{
	id: "wdc002",
	name: "Python-Django",
	description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ",
	price: 45000,
	onOffer: true,

},
{
	id: "wdc003",
	name: "Java-Springboot",
	description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ",
	price: 45000,
	onOffer: true,

}
]

export default coursesData;