import { UserProvider } from './UserContext';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap'
// Web Pages
import CourseView from "./components/CourseView";
import Login from "./pages/Login";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import AppNavbar from "./pages/AppNavbar";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import './App.css';
import { BrowserRouter as Router,Routes, Route, Navigate } from 'react-router-dom';


function App () {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
    console.log(localStorage);
  },[user])

return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} />
          <Route path='/courses/:courseId' element={<CourseView />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route 
            path='/register' 
            element={
              (user.id===null)? (
                <Register />
                ) : (
                  <Navigate to = "/courses"/>
                )
              } 
            />
          <Route path='*' element={<Error />} />
        </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;