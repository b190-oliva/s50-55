import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Container fluid className = "px-5">
			<Row className="mt-3 mb-3">
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Learn from Home</h2>
							</Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ornare nulla est, vel suscipit ante cursus vitae. Donec ultricies est non nunc cursus dapibus sed vitae dolor. Suspendisse dolor risus, porttitor posuere bibendum non, tincidunt et nibh. Quisque eget dolor vitae arcu ullamcorper rutrum posuere auctor dui.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Study Now, Pay Later</h2>
							</Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ornare nulla est, vel suscipit ante cursus vitae. Donec ultricies est non nunc cursus dapibus sed vitae dolor. Suspendisse dolor risus, porttitor posuere bibendum non, tincidunt et nibh. Quisque eget dolor vitae arcu ullamcorper rutrum posuere auctor dui.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className="cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Be Part of Our Community</h2>
							</Card.Title>
							<Card.Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ornare nulla est, vel suscipit ante cursus vitae. Donec ultricies est non nunc cursus dapibus sed vitae dolor. Suspendisse dolor risus, porttitor posuere bibendum non, tincidunt et nibh. Quisque eget dolor vitae arcu ullamcorper rutrum posuere auctor dui.</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
