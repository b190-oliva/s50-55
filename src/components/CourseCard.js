import { Card, Button, Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {   
	const {name, description, price, _id} = courseProp;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	const [isOpen, setIsOpen] = useState(true);

    return (
    	<Container fluid className ="d-flex justify-content-center mt-5">
	        <Card className = "w-50">
	            <Card.Body>
	                <Card.Title>{name}</Card.Title>
	                <Card.Subtitle>Description:</Card.Subtitle>
	                <Card.Text>{description}</Card.Text>
	                <Card.Subtitle>Price:</Card.Subtitle>
	                <Card.Text>{price}</Card.Text>
	                <Card.Text>Enrollees: {count}</Card.Text>
	                <Link className="btn btn-primary" to ={`/courses/${_id}`}>Details</Link>
	            </Card.Body>
	        </Card>
        </Container>
    )
}